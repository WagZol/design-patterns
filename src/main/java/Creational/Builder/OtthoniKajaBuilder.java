package Creational.Builder;

public class OtthoniKajaBuilder implements Builder {
    private String kaja;

    public void createKaja() {
        kaja = "";
    }

    public String getKaja() {
        return kaja;
    }

    public void buildZsemle() {
        kaja += "kenyer";
    }

    public void buildHus() {
        kaja = "parizeres " + kaja;
    }

    public void buildAdalek() {
        kaja = "paprikas " + kaja;
    }

    public void buildUdito() {
        kaja += " teaval";
    }
}
