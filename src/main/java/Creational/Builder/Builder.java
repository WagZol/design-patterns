package Creational.Builder;

public interface Builder {
    public void createKaja();
    public String getKaja();
    public void buildZsemle();
    public void buildHus();
    public void buildAdalek();
    public void buildUdito();
}
