package Creational.Builder;

public class Director {
    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    public String buildSzendvics() {
        builder.createKaja();
        builder.buildZsemle();
        builder.buildHus();
        return builder.getKaja();
    }

    public String buildHamburger() {
        builder.createKaja();
        builder.buildZsemle();
        builder.buildHus();
        builder.buildAdalek();
        return builder.getKaja();
    }

    public String buildMenu() {
        builder.createKaja();
        builder.buildZsemle();
        builder.buildHus();
        builder.buildAdalek();
        builder.buildUdito();
        return builder.getKaja();
    }
}
