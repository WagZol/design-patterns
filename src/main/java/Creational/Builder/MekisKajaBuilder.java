package Creational.Builder;

public class MekisKajaBuilder implements Builder {
    private String kaja;

    public void createKaja() {
        kaja = "";
    }

    public String getKaja() {
        return kaja;
    }

    public void buildZsemle() {
        kaja += "buci";
    }

    public void buildHus() {
        kaja = "huspogacsas " + kaja;
    }

    public void buildAdalek() {
        kaja = "sajtos hagymas szoszos " + kaja;
    }

    public void buildUdito() {
        kaja += " cola-val";
    }
}
