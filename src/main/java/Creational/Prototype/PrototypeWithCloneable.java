package Creational.Prototype;

public class PrototypeWithCloneable implements Cloneable {
    private int property;
    private int[] array;

    int getProperty() {
        return property;
    }

    void setProperty(int property) {
        this.property = property;
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    void incrementArrayFirstElement() {
        ++array[0];
    }

    public Cloneable clone() {
        Cloneable p = null;
        try {
            p = (Cloneable) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return p;
    }
}
