package Creational.Prototype;

interface Prototype {
    Prototype clone();
}
