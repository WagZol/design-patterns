package Creational.Prototype;

public class PrototypeWithCopyConstructor implements Prototype {
    private int property;
    private int[] array;

    int getProperty() { return property; }

    void setProperty(int property) {
        this.property = property;
    }

    int[] getArray() {
        return array;
    }

    void setArray(int[] array) {
        this.array = array;
    }

    void incrementArrayFirstElement() {
        ++array[0];
    }

    PrototypeWithCopyConstructor() {}

    private PrototypeWithCopyConstructor(PrototypeWithCopyConstructor prototype) {
        this.property = prototype.property;
        if (prototype.array.length - 1 >= 0) {
            array = new int[prototype.array.length];
            System.arraycopy(prototype.array, 0, array, 0, prototype.array.length);
        }
    }

    public Prototype clone() {
        return (Prototype) new PrototypeWithCopyConstructor(this);
    }
}
