package Creational.Factory.FactoryMethod;

import Creational.Factory.Product;
import Creational.Factory.ProductA;

class ProductAFactory extends ProductFactory {
    protected Product createProduct() { return new ProductA(); }
}
