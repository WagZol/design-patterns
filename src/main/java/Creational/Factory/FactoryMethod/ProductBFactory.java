package Creational.Factory.FactoryMethod;

import Creational.Factory.Product;
import Creational.Factory.ProductB;

class ProductBFactory extends ProductFactory {
    protected Product createProduct() { return new ProductB(); }
}
