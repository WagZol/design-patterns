package Creational.Factory.FactoryMethod;

import Creational.Factory.Product;

abstract class ProductFactory {
    abstract protected Product createProduct();

    String createProductAndGetName() {
        Product p = createProduct();
        return p.getName();
    }
}
