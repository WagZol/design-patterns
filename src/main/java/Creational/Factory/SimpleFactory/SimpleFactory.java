package Creational.Factory.SimpleFactory;

import Creational.Factory.Product;
import Creational.Factory.ProductA;
import Creational.Factory.ProductB;

class SimpleFactory {
    private Product createProduct(char id) throws IllegalArgumentException {
        switch (id) {
            case 'A':
                return new ProductA();
            case 'B':
                return new ProductB();
            default:
                throw new IllegalArgumentException("ID is not a legal identifier");
        }
    }

    String createProductAndGetName(char id) throws IllegalArgumentException {
        Product p = createProduct(id);
        return p.getName();
    }
}
