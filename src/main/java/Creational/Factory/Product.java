package Creational.Factory;

public interface Product {
    String getName();
}
