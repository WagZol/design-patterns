package Creational.Factory.AbstractFactory;

import Creational.Factory.AnotherProduct;
import Creational.Factory.AnotherProductA;
import Creational.Factory.Product;
import Creational.Factory.ProductA;

class FactoryA implements Factory {
    public Product createProduct() {
        return new ProductA();
    }

    public AnotherProduct createAnotherProduct() { return new AnotherProductA(); }
}
