package Creational.Factory.AbstractFactory;

import Creational.Factory.AnotherProduct;
import Creational.Factory.AnotherProductB;
import Creational.Factory.Product;
import Creational.Factory.ProductB;

public class FactoryB implements Factory {
    public Product createProduct() {
        return new ProductB();
    }

    public AnotherProduct createAnotherProduct() { return new AnotherProductB(); }
}
