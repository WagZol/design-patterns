package Creational.Factory.AbstractFactory;

import Creational.Factory.AnotherProduct;
import Creational.Factory.Product;

interface Factory {
    Product createProduct();
    AnotherProduct createAnotherProduct();
}
