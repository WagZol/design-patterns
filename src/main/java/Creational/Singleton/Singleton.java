package Creational.Singleton;

class Singleton {
    private static Singleton instance;
    static int constructorCounter;

    private Singleton() {
        super();
        ++constructorCounter;
    }

    static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
