package Behavioral.ChainOfResponsibility;

public abstract class Handler {
    private Handler next;

    public void setNext(Handler next) {
        this.next = next;
    }

    public String handle(String input) {
        if(next != null) {
            return next.handle(input);
        }
        return "default value";
    }
}
