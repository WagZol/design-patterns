package Behavioral.ChainOfResponsibility;

public class ConcreteHandlerB extends Handler {

    public String handle(String input) {
        if (input.equals("accepted parameter for B")) {
            return "handled by B";
        }
        return super.handle(input);
    }
}
