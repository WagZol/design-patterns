package Behavioral.ChainOfResponsibility;

public class ConcreteHandlerA extends Handler {

    public String handle(String input) {
        if (input.equals("accepted parameter for A")) {
            return "handled by A";
        }
        return super.handle(input);
    }
}
