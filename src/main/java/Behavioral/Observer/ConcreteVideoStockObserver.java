package Behavioral.Observer;

import java.util.List;

public class ConcreteVideoStockObserver implements VideoObserver, StockObserver {

    private String latestVideo = "";
    private boolean isInStock = false;

    public void notify(VideoObservable observable) {
        List<String> videos = observable.getVideos();
        latestVideo = videos.get(videos.size() - 1);
    }

    public void notify(StockObservable observable) {
        isInStock = observable.isInStock();
    }

    public String getLatestVideo() {
        return latestVideo;
    }

    public boolean isInStock() {
        return isInStock;
    }
}
