package Behavioral.Observer;

import java.util.ArrayList;
import java.util.List;

public abstract class VideoObservable {

    private List<VideoObserver> observers = new ArrayList<VideoObserver>();

    public void register(VideoObserver observer) {
        observers.add(observer);
    }

    public void unregister(VideoObserver observer) {
        observers.remove(observer);
    }

    void notifyObservers() {
        for (VideoObserver observer : observers) {
            observer.notify(this);
        }
    }

    abstract List<String> getVideos();
}
