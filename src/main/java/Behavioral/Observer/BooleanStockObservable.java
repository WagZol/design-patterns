package Behavioral.Observer;

public class BooleanStockObservable extends StockObservable {

    private boolean inStock = false;

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
        notifyObservers();
    }

    boolean isInStock() {
        return inStock;
    }
}
