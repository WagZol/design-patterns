package Behavioral.Observer;

import java.util.ArrayList;
import java.util.List;

public class YoutubeVideoObservable extends VideoObservable {

    private List<String> newYouTubeVideos = new ArrayList<String>();

    List<String> getVideos() {
        return newYouTubeVideos;
    }

    public void addYouTubeVideo(String title) {
        this.newYouTubeVideos.add(title);
        this.notifyObservers();
    }
}
