package Behavioral.Observer;

public interface StockObserver {
    void notify(StockObservable observable);
}
