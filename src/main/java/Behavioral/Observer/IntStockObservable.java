package Behavioral.Observer;

public class IntStockObservable extends StockObservable {

    private int inStock = 0;

    public void setInStock(int inStock) {
        this.inStock = inStock;
        notifyObservers();
    }

    boolean isInStock() {
        return inStock > 0;
    }
}
