package Behavioral.Observer;

import java.util.ArrayList;
import java.util.List;

public abstract class StockObservable {

    private List<StockObserver> observers = new ArrayList<StockObserver>();

    public void register(StockObserver observer) {
        observers.add(observer);
    }

    public void unregister(StockObserver observer) {
        observers.remove(observer);
    }

    void notifyObservers() {
        for (StockObserver observer : observers) {
            observer.notify(this);
        }
    }

    abstract boolean isInStock();
}
