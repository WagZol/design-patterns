package Behavioral.Observer;

import java.util.List;

public class ConcreteVideoObserver implements VideoObserver {

    private String latestVideo = "";

    public void notify(VideoObservable observable) {
        List<String> videos = observable.getVideos();
        latestVideo = videos.get(videos.size() - 1);
    }

    public String getLatestVideo() {
        return latestVideo;
    }
}
