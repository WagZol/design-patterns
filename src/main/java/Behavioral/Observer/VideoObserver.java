package Behavioral.Observer;

public interface VideoObserver {
    void notify(VideoObservable observable);
}
