package Behavioral.Observer;

import java.util.ArrayList;
import java.util.List;

public class VimeoVideoObservable extends VideoObservable {
    
    private List<String> newVimeoVideos = new ArrayList<String>();

    List<String> getVideos() {
        return newVimeoVideos;
    }

    public void addVimeoVideo(String title) {
        this.newVimeoVideos.add(title);
        this.notifyObservers();
    }
}
