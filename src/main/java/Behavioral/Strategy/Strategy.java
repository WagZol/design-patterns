package Behavioral.Strategy;

public interface Strategy {
    int getResult(int a, int b);
}
