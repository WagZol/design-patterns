package Behavioral.Strategy;

public class Calculator {
    private Strategy operator;
    private int a, b;

    public int getResult() {
        return operator.getResult(a, b);
    }

    public void setOperands(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public void setOperator(Strategy operator) {
        this.operator = operator;
    }
}
