package Behavioral.Strategy;

public class SubstractionStrategy implements Strategy {
    public int getResult(int a, int b) {
        return a - b;
    }
}
