package Behavioral.Strategy;

public class AdditionStrategy implements Strategy {
    public int getResult(int a, int b) {
        return a + b;
    }
}
