package Behavioral.ChainOfResponsibility;

import Behavioral.ChainOfResponsibility.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ChainOfResponsibilityTest {

    @Test
    public void testChainOfResponsibility() {
        Handler handlerA = new ConcreteHandlerA();
        Handler handlerB = new ConcreteHandlerB();
        handlerA.setNext(handlerB);

        Assert.assertEquals(handlerA.handle("accepted parameter for A"), "handled by A");
        Assert.assertEquals(handlerA.handle("accepted parameter for B"), "handled by B");
        Assert.assertEquals(handlerA.handle("anything else"), "default value");
    }
}