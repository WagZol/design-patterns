package Behavioral.Strategy;

import Behavioral.Observer.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class StrategyTest {

    @Test
    public void testStrategy() {
        Calculator calculator = new Calculator();
        calculator.setOperands(3, 2);

        calculator.setOperator(new AdditionStrategy());
        Assert.assertEquals(calculator.getResult(), 5);

        calculator.setOperator(new SubstractionStrategy());
        Assert.assertEquals(calculator.getResult(), 1);
    }
}