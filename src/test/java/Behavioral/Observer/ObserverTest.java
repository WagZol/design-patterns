package Behavioral.Observer;

import Behavioral.Observer.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ObserverTest {

    @Test
    public void testObserver() {
        YoutubeVideoObservable youtube = new YoutubeVideoObservable();
        VimeoVideoObservable vimeo = new VimeoVideoObservable();
        BooleanStockObservable booleanStock = new BooleanStockObservable();
        IntStockObservable intStock = new IntStockObservable();

        ConcreteVideoObserver videoObserver = new ConcreteVideoObserver();
        youtube.register(videoObserver);
        vimeo.register(videoObserver);

        ConcreteVideoStockObserver youtubeStockObserver = new ConcreteVideoStockObserver();
        youtube.register(youtubeStockObserver);
        intStock.register(youtubeStockObserver);
        booleanStock.register(youtubeStockObserver);

        youtube.addYouTubeVideo("Godzilla");
        Assert.assertEquals(videoObserver.getLatestVideo(), "Godzilla");
        Assert.assertEquals(youtubeStockObserver.getLatestVideo(), "Godzilla");

        vimeo.addVimeoVideo("Superman");
        Assert.assertEquals(videoObserver.getLatestVideo(), "Superman");
        Assert.assertEquals(youtubeStockObserver.getLatestVideo(), "Godzilla");

        intStock.setInStock(0);
        Assert.assertFalse(youtubeStockObserver.isInStock());
        intStock.setInStock(1);
        Assert.assertTrue(youtubeStockObserver.isInStock());

        booleanStock.setInStock(false);
        Assert.assertFalse(youtubeStockObserver.isInStock());
    }
}