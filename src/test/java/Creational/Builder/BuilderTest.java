package Creational.Builder;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BuilderTest {

    @Test
    public void testBuilder() {
        Builder konyha = new OtthoniKajaBuilder();
        Builder mcDonalds = new MekisKajaBuilder();

        Director anyu = new Director(konyha);
        Director bohoc = new Director(mcDonalds);

        Assert.assertEquals(anyu.buildSzendvics(), "parizeres kenyer");
        Assert.assertEquals(anyu.buildHamburger(), "paprikas parizeres kenyer");
        Assert.assertEquals(anyu.buildMenu(), "paprikas parizeres kenyer teaval");

        Assert.assertEquals(bohoc.buildSzendvics(), "huspogacsas buci");
        Assert.assertEquals(bohoc.buildHamburger(), "sajtos hagymas szoszos huspogacsas buci");
        Assert.assertEquals(bohoc.buildMenu(), "sajtos hagymas szoszos huspogacsas buci cola-val");
    }


    @Test
    public void testBuilderNemHasznalatoKesobbiModositasra() {
        Builder konyha = new OtthoniKajaBuilder();
        Director anyu = new Director(konyha);

        String kaja = anyu.buildSzendvics();
        Assert.assertEquals(kaja, "parizeres kenyer");

        konyha.buildUdito();

        String modositottKaja = konyha.getKaja();
        Assert.assertEquals(modositottKaja, "parizeres kenyer teaval");
        Assert.assertEquals(kaja, "parizeres kenyer");
    }
}