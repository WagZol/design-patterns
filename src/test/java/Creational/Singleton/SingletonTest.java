package Creational.Singleton;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SingletonTest {

    @Test
    public void testConstructorExecutedOnlyOnce() {
        Singleton s;

        s = Singleton.getInstance();
        Assert.assertEquals(1, Singleton.constructorCounter);

        s = Singleton.getInstance();
        Assert.assertEquals(1, Singleton.constructorCounter);
    }

}