package Creational.Factory.FactoryMethod;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FactoryMethodTest {

    @Test
    public void testProductACreated() {
        ProductFactory f = new ProductAFactory();
        Assert.assertEquals("ProductA", f.createProductAndGetName());
    }

    @Test
    public void testProductBCreated() {
        ProductFactory f = new ProductBFactory();
        Assert.assertEquals("ProductB", f.createProductAndGetName());
    }
}