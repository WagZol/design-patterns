package Creational.Factory.SimpleFactory;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SimpleFactoryTest {

    @Test
    public void testProductACreated() throws IllegalArgumentException {
        SimpleFactory f = new SimpleFactory();
        Assert.assertEquals("ProductA", f.createProductAndGetName('A'));
    }

    @Test
    public void testProductBCreated() throws IllegalArgumentException {
        SimpleFactory f = new SimpleFactory();
        Assert.assertEquals("ProductB", f.createProductAndGetName('B'));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testExceptionOnWrongID() throws IllegalArgumentException {
        SimpleFactory f = new SimpleFactory();
        f.createProductAndGetName('X');
    }
}