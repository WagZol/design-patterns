package Creational.Factory.AbstractFactory;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AbstractFactoryTest {

    @Test
    public void testACreated() {
        Factory f = new FactoryA();
        Assert.assertEquals("ProductA", f.createProduct().getName());
        Assert.assertEquals("AnotherProductA", f.createAnotherProduct().getName());
    }

    @Test
    public void testBCreated() {
        Factory f = new FactoryB();
        Assert.assertEquals("ProductB", f.createProduct().getName());
        Assert.assertEquals("AnotherProductB", f.createAnotherProduct().getName());
    }
}