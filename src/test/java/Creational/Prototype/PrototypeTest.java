package Creational.Prototype;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PrototypeTest {

    @Test
    void testPrototypeWithCopyConstructorCanDeepCopy() {
        PrototypeWithCopyConstructor p = new PrototypeWithCopyConstructor();
        int[] p_array = new int[3];
        p_array[0] = 0;
        p_array[1] = 1;
        p_array[2] = 2;
        p.setArray(p_array);
        p.setProperty(1);

        PrototypeWithCopyConstructor c = (PrototypeWithCopyConstructor) p.clone();

        Assert.assertNotEquals(p, c);
        Assert.assertEquals(p.getProperty(), c.getProperty());
        Assert.assertEquals(p.getArray(), c.getArray());

        c.setProperty(2);
        Assert.assertEquals(1, p.getProperty());
        Assert.assertEquals(2, c.getProperty());

        c.incrementArrayFirstElement();
        Assert.assertNotEquals(p, c);
        Assert.assertEquals(0, p.getArray()[0]);
        Assert.assertEquals(1, c.getArray()[0]);
    }

    @Test
    void testPrototypeWithObjectCloneIsShallowCopy() {
        PrototypeWithCloneable p = new PrototypeWithCloneable();
        int[] p_array = new int[3];
        p_array[0] = 0;
        p_array[1] = 1;
        p_array[2] = 2;
        p.setArray(p_array);
        p.setProperty(1);

        PrototypeWithCloneable c = (PrototypeWithCloneable) p.clone();

        Assert.assertNotEquals(p, c);
        Assert.assertEquals(p.getProperty(), c.getProperty());
        Assert.assertEquals(p.getArray(), c.getArray());

        c.setProperty(2);
        Assert.assertEquals(1, p.getProperty());
        Assert.assertEquals(2, c.getProperty());

        // WARNING: Object.clone is shallow copy, both object has the same array!

        c.incrementArrayFirstElement();
        Assert.assertNotEquals(p, c);
        Assert.assertEquals(1, p.getArray()[0]);
        Assert.assertEquals(1, c.getArray()[0]);
    }
}